import { Component, OnInit } from '@angular/core';
import { ServSuperHeroService } from '../serv-super-hero.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-selection-page',
  templateUrl: './selection-page.page.html',
  styleUrls: ['./selection-page.page.scss'],
})
export class SelectionPagePage implements OnInit {
  random : any; 
  heroID : any;
  heroName : any;
  heroImage : any;
  heroPowerData :any;
  heroBiography: any;
  heroAppearance : any;
  heroCatalogue : any[] = [];
  heroIDs = [];
  //////////////////
  chosen : number;
  constructor(public service:ServSuperHeroService,
              private alertCtrl:AlertController) { }

  ngOnInit(){
    this.heroID = this.random;
    console.log('ID a agregar:', this.heroID);
    this.service.getHero(this.heroID).subscribe((data)=>{
      this.heroName = data['name'];
      this.heroImage = data['image'];
    })
  }

  generateRandomNumber(){
    this.random = Math.floor(Math.random() * (731) + 1);
    this.ngOnInit();
  }

  addHero(){
    this.heroIDs.push(this.heroID);
    alert('El personaje ' + this.heroName + ' Se ha agregado');
    this.service.setStoragedHeroes(this.heroIDs);
    this.generateRandomNumber();
  }
}
