import { Component } from '@angular/core';
import { ServSuperHeroService } from '../serv-super-hero.service';
import { NavController, IonRefresher } from '@ionic/angular';
import { RefresherEventDetail } from '@ionic/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  heroes = [];
  heroesIDs = [];
  heroesNames = [];
  heroesImage = [];
  heroesPowerStats = [];
  heroesAppearance = [];
  index : any;
  constructor(public service:ServSuperHeroService,
              public router:Router,
              private alertCTRL:AlertController) {}

  ionViewWillEnter(){
    this.heroesNames = [];
    this.heroesImage = [];
    this.heroesAppearance = [];
    this.heroesPowerStats = [];
    this.heroes = this.service.getStoragedHeroes();
    console.log(this.heroes);
    for(let i=0;i<this.heroes.length;i++){
      this.service.getHero(this.heroes[i]).subscribe(
        (data)=> {
          this.heroesNames.push(data['name']);
          this.heroesImage.push(data['image']);
          this.heroesPowerStats.push(data['powerstats']);
          this.heroesAppearance.push(data['appearance']);
          this.heroesIDs.push(data['id']);
        }
      )
    }
    this.heroes = this.heroesIDs;
    console.log('New array of heroes: ', this.heroesNames);
    console.log('New array of heroes: ', this.heroesNames);
    console.log('New array of heroes: ', this.heroesAppearance);
    console.log('New array of heroes: ', this.heroesPowerStats);
    console.log('New array of indexes: ', this.heroes)
  }

  removeHero(index){
    this.index = index;
    this.alertP();
  }

  async alertP(){
    const alert = await this.alertCTRL.create({
      header: '¿Estás seguro?',
      message: '¿De verdad quiere eliminar este personaje de la lista?',
      buttons: [{
        text:'No',
        role:'Cancel',
        handler: ()=>{
          console.log (this.heroesNames[this.index], 'no ha sido eliminado');
        }
      },{
        text:'Sí',
        handler: () =>{
        let data = [];
        let dataa = null;
        console.log('Recibido ID: ', this.index);
        console.log('Eliminar: ', this.heroesNames[this.index]);
        for(let i=0;i<this.heroes.length;i++){
          if(this.heroes[i]!=this.index){
            data.push(this.heroes[i]);
            console.log(this.heroesNames[i] ,' agregado');
          } else {
            console.log(this.heroes[i], ' no agregado');
          } 
        }
        console.log('Data: ', data);
        this.heroes = [];
        this.heroesIDs = [];
        this.service.setStoragedHeroes(data);
        this.ionViewWillEnter();
        }
      }]
    });
    await alert.present();
  }
}
