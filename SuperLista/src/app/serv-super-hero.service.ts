import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ServSuperHeroService {
  selectedHeroes = [];
  constructor(private http:HttpClient,
              private storage: Storage) { 
                this.loadStoragedHeroes();
              }
  
  getHero(heroID){
    return this.http.get('https://superheroapi.com/api/543528076525019/'+heroID);
  }

  setStoragedHeroes(newHeroes){
    this.selectedHeroes = newHeroes;
    this.storage.set('storagedHeroes',this.selectedHeroes);
    console.log('Almacenados cambia a: ', this.selectedHeroes);
  }

  getStoragedHeroes(){
    console.log('transmited');
    return this.selectedHeroes;
  }

  loadStoragedHeroes(){
    this.storage.get('storagedHeroes').then((val)=>{
      if(val!=null){
        this.selectedHeroes = val;
      }
    })
  }
}
