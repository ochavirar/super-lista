import { TestBed } from '@angular/core/testing';

import { ServSuperHeroService } from './serv-super-hero.service';

describe('ServSuperHeroService', () => {
  let service: ServSuperHeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServSuperHeroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
